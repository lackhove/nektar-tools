#!/bin/bash

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.

set -e
set -u

dir=$(dirname $1)
case_name=$(basename $1)
case_name="${case_name%.*}"

cd $dir

# backup last run
latest_file=$(find -mindepth 1 -maxdepth 1 -iname "${case_name}_*.rst" | sort -V | tail -n 1)
if [[ -e $latest_file ]]; then
    save_dir="${latest_file%.*}"
    if [[ ! -e $save_dir ]]; then
        mkdir -pv $save_dir
        cp -v $latest_file "${latest_file%.*}.xdmf" ${case_name}*.mon ${case_name}.fal ${case_name}.flx  ${case_name}.pin ${case_name}.res ${case_name}.tps ${case_name}.vol code.tar.gz flamethickness.dat MaxSourceTermPerMixFr.dat precise.log precise-uns $save_dir || true
        ln -s ../${case_name}.geo $save_dir/${case_name}.geo
        gzip $save_dir/precise.log || true
    fi

    rst_file="${case_name}.rst"
    rm -vf "$rst_file" 
    ln -vs "$latest_file" "$rst_file"
fi

# concat and cleanup logfiles
first_logfile=$(find -mindepth 1 -maxdepth 1 -iname "log.*" | sort -V | head -n 1)
if [[ ! -e $first_logfile && $(find ../ -maxdepth 1 -iname "log.*" | wc -l) -ge 1 ]]; then
    first_logfile=$(grep -l "executable.*precise-uns" $(find ../ -maxdepth 1 -iname "log.*") | sort -u | head -n 1)
fi

if [[ -e $first_logfile ]]; then
    echo "appending log"
    cat "$first_logfile" >> precise.log
    rm -vf log.*
    rm -vf $(grep -l "executable.*precise-uns" $(find ../ -maxdepth 1 -iname "log.*"))
fi

# write slurm job id
if [ ! -z ${SLURM_JOBID+x} ]; then
    echo "writing job ID ${SLURM_JOBID} to precise.log"
    echo "" >> precise.log
    echo "new run, SLURM job ID: ${SLURM_JOBID}" >> precise.log
fi
