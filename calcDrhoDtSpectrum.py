#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.



import sys
import logging
import argparse

import numpy as np
import h5py

from plotSpectra import time_to_freq

logger = logging.getLogger('root')

def main():
    parser = argparse.ArgumentParser(
        description='Compute the spectrum from a PRECISE-UNS flx file')
    parser.add_argument('--debug', action='store_true', dest='debug')

    parser.add_argument('in_file', type=str, help='flx file name')
    parser.add_argument('dt', type=float, help='dt')
    parser.add_argument('out_file', type=str, help='outfile name')
    parser.add_argument('--padfac', action='store', type=int, default=1, help="Padding factor (default: %(default)s)")
    parser.add_argument('--window', action='store', type=str, default='hamming',
                        help='Window function to apply. Either "hanning", "hamming", "bartlett", "blackman" or "boxcar" (default: %(default)s)')
    parser.add_argument('--cutoff', action='store', type=float, help="Cutoff frequency (default: no cutoff)")
    parser.add_argument('--steps', type=float, nargs=2, help='use steps from n1 to n1 (default: %(default)s)')

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )

    spectra_file = h5py.File(args.out_file, "w")
    spectra_group = spectra_file.create_group("spectra")

    time_data = np.genfromtxt(
            args.in_file,
            names=True,
        )

    print(time_data)

    if args.steps:
        try:
            args.steps = [int(k) for k in args.steps]
        except:
            print("--steps does not contain valid integers")
            sys.exit(-1)
    else:
        args.steps = [time_data['Iterations'][0], time_data['Iterations'][-1]]

    start_idx = np.searchsorted(time_data['Iterations'], args.steps[0])
    end_idx = np.searchsorted(time_data['Iterations'], args.steps[1])
    time_data = time_data[start_idx:end_idx]

    print("using iterations {} to {}".format(time_data['Iterations'][0], time_data['Iterations'][-1]))

    f = time_to_freq(time_data, pad_fac=args.padfac, window=args.window, cutoff=args.cutoff, var_name="Outflow", dt=args.dt)
    spectra_group.create_dataset('0', data=f)

    spectra_file.close()


if __name__ == "__main__":
    main()
