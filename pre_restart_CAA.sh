#!/bin/bash

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.

set -e
set -u

dir=$(dirname $1)
case_name=$(basename $1)
case_name="${case_name%.*}"

cd $dir

# backup last run
latest_file=$(find -mindepth 1 -maxdepth 1 -iname "${case_name}_*.chk" | sort -V | tail -n 1)
rm -vf "${case_name}.xml"
if [[ -e $latest_file ]]; then
    ln -vs "${case_name}.restart.xml" "${case_name}.xml"

    rst_file="${case_name}.rst"
    rm -rfv "$rst_file"
    cp -var "$latest_file" "$rst_file"

    if [[ -e "${case_name}.his" ]]; then
        mv -v --backup=t "${case_name}.his" "${latest_file%.*}.his"
    fi
else
    ln -vs "${case_name}.init.xml" "${case_name}.xml"
fi

# concat and cleanup logfiles
first_logfile=$(find -mindepth 1 -maxdepth 1 -iname "log.*" | sort -V | head -n 1)
if [[ ! -e $first_logfile && $(find ../ -maxdepth 1 -iname "log.*" | wc -l) -ge 1 ]]; then
    first_logfile=$(grep -l "executable.*nektar.*Solver" $(find ../ -maxdepth 1 -iname "log.*") | sort -u | head -n 1)
fi

if [[ -e $first_logfile ]]; then
    echo "appending log"
    cat "$first_logfile" >> nektar.log
    rm -vf log.*
    rm -vf $(grep -l "executable.*nektar.*Solver" $(find ../ -maxdepth 1 -iname "log.*"))
fi

# write slurm job id
if [ ! -z ${SLURM_JOBID+x} ]; then
    echo "writing job ID ${SLURM_JOBID} to nektar.log"
    echo "" >> nektar.log
    echo "new run, SLURM job ID: ${SLURM_JOBID}" >> nektar.log
fi


# prepare this run
if [[ ! -e ".petscrc" ]]; then
    echo "ERROR: no petscrc found!"
    exit -1
fi
