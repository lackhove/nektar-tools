#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.


import shutil
import argparse
import logging
import sys
import os

import numpy as np

logger = logging.getLogger('root')


def main():
    global args

    parser = argparse.ArgumentParser(
        description='Converts a paraview csv file to Nektar++ pts format')
    parser.add_argument('file_csv', type=str, help='input csv file')
    parser.add_argument('file_pts', type=str, help='outnput pts file')
    parser.add_argument(
        '--dim', type=int, default=3, help='dimension (default = 3)')
    parser.add_argument('--pressure', type=float, default=1E5,
                        help='Base pressure [Pa] (default = 1E5 Pa)')
    parser.add_argument('--pressure-name', type=str, default='P',
                        help='Name of the pressure field (default: \'pressure\'')
    parser.add_argument(
        '-f', '--field', type=str, action='append', help='process extra field')
    parser.add_argument('--force', action='store_true')
    parser.add_argument('--debug', action='store_true', dest='debug')
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )

    if not args.force and os.path.exists(args.file_pts):
        print(
            'ERROR: {} already exists. Use --force to overwrite.'.format(args.file_pts))
        return -1

    data = np.genfromtxt(args.file_csv, delimiter=",", names=True, unpack=True)

    print("fields:")
    for f in data.dtype.names:
        print("\t{}".format(f))

    cp_cols = ['Points0']
    if args.dim > 1:
        cp_cols.append('Points1')
    if args.dim > 2:
        cp_cols.append('Points2')

    cp_cols.append('U')
    if args.dim > 1:
        cp_cols.append('V')
    if args.dim > 2:
        cp_cols.append('W')

    cp_cols.extend([args.pressure_name, 'Den'])
    if args.field:
        cp_cols.extend(args.field)

    print(cp_cols)

    data = data[cp_cols]

    data[args.pressure_name] += args.pressure

    for col in cp_cols:
        print("mean({}) = {}".format(col, np.mean(data[col])))

    for c in cp_cols:
        if not c in data.dtype.names:
            print('ERROR: Field {} not found in {}'.format(c, args.file_csv))
            return -1

    with open(args.file_pts, 'w') as fn:
        print('<?xml version="1.0" encoding="utf-8"?>', file=fn)
        print('<NEKTAR>', file=fn)
        print(
            '  <POINTS DIM="{}" FIELDS="{}">'.format(args.dim, ','.join(cp_cols)), file=fn)
        np.savetxt(fname=fn, X=data, delimiter=' ')
        print('  </POINTS>', file=fn)
        print('</NEKTAR>', file=fn)


if __name__ == "__main__":
    main()
