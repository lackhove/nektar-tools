#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.




import re
import sys
import os
import argparse
import tempfile
import logging
import natsort
from itertools import cycle
from cycler import cycler

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import h5py

from joinHdf5 import open_mic_files, join_sections

logger = logging.getLogger(__name__)

def get_styles():

    dashes = cycler(dashes=[
        (None, None),
        (8, 1),
        (4, 1),
        (2, 1),
        (4, 1, 1, 1),
        (4, 1, 2, 1),
        (4, 1, 1, 1, 1, 1),
    ])

    return dashes * mpl.rcParams['axes.prop_cycle']


def plot_discont(args, ax, styles, mic_groups, n_mic):

    for ii, group in enumerate(mic_groups):

        for var in args.var:

            sty = next(styles)

            if args.steps:
                ax.plot(
                    group[n_mic][var],
                    label="{} #{}, {}".format(var, n_mic, args.mic_files[ii]),
                    alpha=0.5,
                    color=sty['color'],
                    dashes=sty['dashes'],
                )
            else:
                ax.plot(
                    group[n_mic]['time'],
                    group[n_mic][var],
                    label="{} #{}, {}".format(var, n_mic, args.mic_files[ii]),
                    alpha=0.5,
                    color=sty['color'],
                    dashes=sty['dashes'],
                )

def plot_cont(args, ax, styles, mic_groups, n_mic):

    joint_mic = join_sections(mic_groups, int(n_mic))

    for var in args.var:

        sty = next(styles)

        if args.steps:
            ax.plot(
                joint_mic[var],
                label="{} #{}".format(var, n_mic),
                alpha=0.5,
                color=sty['color'],
                dashes=sty['dashes'],
            )
        else:
            ax.plot(
                joint_mic['time'],
                joint_mic[var],
                label="{} #{}".format(var, n_mic),
                alpha=0.5,
                color=sty['color'],
                dashes=sty['dashes'],
            )


def plot_timedata(args, mic_groups):

    plt.style.use(args.style)

    if 'beamer' in args.style:
        # full beamer page
        fs = (0.25, 0.138)
        fs = (fs[0], fs[0] / 1.8209)
        fs = (fs[0] / 0.0254, fs[1] / 0.0254)
    else:
        # tudreport with 1:2
        fs = (0.17499796, 0.2464048)
        fs = (fs[0], fs[0] * 0.5)
        fs = (fs[0] / 0.0254, fs[1] / 0.0254)

    fig = plt.figure(figsize=(fs[0], fs[1]))

    ax = fig.add_subplot(111)

    styles = cycle(get_styles())
    for n_mic in mic_groups[0]:

        if args.mic_num:
            if not int(n_mic) in args.mic_num:
                continue

        if args.discont:
            plot_discont(args, ax, styles, mic_groups, n_mic)
        else:
            plot_cont(args, ax, styles, mic_groups, n_mic)

    (ymin, ymax) = ax.get_ylim()
    (xmin, xmax) = ax.get_xlim()
    xmin = max(xmin, 0)
    if args.xmin:
        xmin = args.xmin
    if args.xmax:
        xmax = args.xmax
    if args.ymin:
        ymin = args.ymin
    if args.ymax:
        ymax = args.ymax

    ax.set(
        xlabel=r'$t$ [s]',
        ylim=(ymin, ymax),
        xlim=(xmin, xmax),
    )
    if args.steps:
        ax.set(xlabel=r'steps [-]')

    ax.set(ylabel='$p$ [Pa]')

    #majorFormatter = mpl.ticker.FormatStrFormatter('%.4G')
    # ax.xaxis.set_major_formatter(majorFormatter)
    ax.grid(True)

    plt.tight_layout()

     # Shrink current axis by 20%
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

    ax.legend(bbox_to_anchor=(1, 1), loc='upper left')

    if args.out:
        print('writing {0}'.format(args.out))
        plt.savefig(args.out, dpi=300)

    if args.show:
        plt.show()


def main():
    parser = argparse.ArgumentParser(
        description='plots the HistoryPoints values over time')
    parser.add_argument('mic_files', nargs='+', type=str, help='mic file (hdf5)')
    parser.add_argument(
        '--mic-num', type=int, nargs='*', help='mic numbers to plot')
    parser.add_argument('--out', '-o', action='store', type=str)
    parser.add_argument('--show', '-s', action='store_true')
    parser.add_argument('--style', action='store',
                        default='default', help="plot style (default: %(default)s)")
    parser.add_argument('--steps', action='store_true')
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--discont', action='store_true', help="discontinuous plot")
    parser.add_argument('--xmin', type=float)
    parser.add_argument('--xmax', type=float)
    parser.add_argument('--ymin', type=float)
    parser.add_argument('--ymax', type=float)
    parser.add_argument('--var', nargs='+', default='p')

    args = parser.parse_args()

    if not args.show and not args.out:
        args.show = True

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            level=logging.INFO,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )

    args.mic_files = natsort.natsorted(args.mic_files)

    mics, mic_handles = open_mic_files(args.mic_files)

    vars = mics[0]['0'].dtype.names
    print("available vars: {}".format(', '.join(vars)))

    for v in args.var:
        if v not in vars:
            print("\"{}\" not in vars".format(args.var))
            sys.exit(-1)

    plot_timedata(args, mics)

    for fh in mic_handles:
        fh.close()

if __name__ == "__main__":
    main()
