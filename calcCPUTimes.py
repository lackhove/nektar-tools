#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.



import re
import os
import sys
import logging
import argparse
from itertools import cycle
from cycler import cycler

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy import stats

logger = logging.getLogger('root')

class Run:

    def __init__(self):

        self.step = []
        self.t = []
        self.t_cpu = []
        self.t_waitR = []
        self.t_waitS = []
        self.t_pp = []
        self.dt = None
        self.stride = None
        self.t_start = None
        self.hosts = {}
        self.slurm_id = None
        self.data = None
        self.sender = False
        self.receiver = False


def readfiles(filenames):
    for fn in filenames:
        with open(fn) as f:
            for line in f:
                yield line


def drop_runs(runs, var_name='step'):
    # drop runs which are subseeded by sections starting earlier than themselves
    while True:
        droplist = []
        for ii in range(len(runs) - 1):
            if runs[ii].data[var_name][0] >= runs[ii + 1].data[var_name][0]:
                droplist.append(ii)

        if not droplist:
            break

        for ii in reversed(droplist):
            runs.pop(ii)

    return runs


def parse_nektar_log(log_files, args):

    runs = []

    for line in readfiles(log_files):

        match = re.search(r'new run, SLURM job ID: (\d+)', line)
        if match:
            runs.append(Run())
            run = runs[-1]
            run.slurm_id = int(match.group(1))

            continue

        match = re.search(r'GlobalSysSoln Info', line)
        if match:

            if not runs or not runs[-1].slurm_id:
                runs.append(Run())
                run = runs[-1]

            t_recv = np.NAN
            t_waitR = np.NAN
            t_waitS = np.NAN
            t_pp = np.NAN

            continue

        if not runs:
            continue

        match = re.search(r'rank (\d+) is running on node (\S+)', line)
        if match:
            r = int(match.group(1))
            h = match.group(2)
            if not h in run.hosts:
                run.hosts[h] = []
            run.hosts[h].append(r)

            continue

        match = re.search(r'\s+Time Step:\s*(\S+)\s*', line)
        if match:
            run.dt = float(match.group(1))

            continue

        match = re.search(
            r'Receive total time:\s*(\S+), Receive waiting time:\s*(\S+)', line)
        if match:
            run.receiver = True
            t_recv = float(match.group(1))
            t_waitR = float(match.group(2))
            t_pp = t_recv - t_waitR

            continue

        match = re.search(
            r'Send total time:\s+(\S+).*', line)
        if match:
            run.sender = True
            t_waitS = float(match.group(1))

            continue

        match = re.search(
            r'Steps:\s*(\S+)\s*Time:\s*(\S+)\s*CPU Time: \s*(\S+)\s*s', line)
        if match:
            step = int(match.group(1))
            t_cpu = float(match.group(3))
            time = float(match.group(2))

            if not run.t_start:
                run.t_start = time

            if len(run.step) > 0:
                run.stride = step - run.step[-1]
            else:
                run.stride = 1


            if run.t_start and run.dt:
                time = run.t_start + step * run.dt

            if not np.isnan(t_recv):
                t_cpu = t_cpu - t_recv

            run.step.append(step)
            run.t.append(time)
            # exclude all steps which FOLLOW the exclude step and the first receive step
            if (args.exclude_every and (step % args.exclude_every == run.stride) or len(run.step) == 1) :
                run.t_cpu.append(np.NAN)
                run.t_waitR.append(np.NAN)
                run.t_waitS.append(np.NAN)
                run.t_pp.append(np.NAN)
            else:
                run.t_cpu.append(t_cpu)
                run.t_waitR.append(t_waitR)
                run.t_waitS.append(t_waitS)
                run.t_pp.append(t_pp)

            t_recv = np.NAN
            t_waitR = np.NAN
            t_waitS = np.NAN
            t_pp = np.NAN

            continue

    runs = [x for x in runs if len(x.step) > 0]

    for run in runs:

        # TODO: int
        run.data = np.zeros(shape=(len(run.step),), dtype=[('step', 'int32'), ('global_step', 'int32'), (
            'time', 'float32'), ('cpu_time', 'float32'), ('waitR_time', 'float32'), ('waitS_time', 'float32'), ('pp_time', 'float32')])

        run.data['step'] = run.step
        run.data['global_step'] = run.step
        run.step = []
        run.data['time'] = run.t
        run.t = []
        run.data['cpu_time'] = run.t_cpu
        run.t_cpu = []
        run.data['waitR_time'] = run.t_waitR
        run.t_waitR = []
        run.data['waitS_time'] = run.t_waitS
        run.t_waitS = []
        run.data['pp_time'] = run.t_pp
        run.t_pp = []

    runs = drop_runs(runs, 'time')

    for ii, run in enumerate(runs[0:-1]):
        runs[ii + 1].data['global_step'] = runs[ii +
                                                1].data['global_step'] + runs[ii].data['global_step'][-1]

    return runs


def parse_precise_log(log_files, args):

    var_order = {'U' : 0,
                 'V' :  1,
                 'W' :     2,
                 'P' : 3,
                 'FMIX' : 4,
                 'GMIX' :    5,
                 'PGR' :        6,
                 'StepTime' : 7,
                 'RunTime' : 8,
                 }

    runs = []

    for line in readfiles(log_files):

        match = re.search(r'new run, SLURM job ID: (\d+)', line)
        if match:
            runs.append(Run())
            run = runs[-1]
            run.slurm_id = int(match.group(1))

            continue

        match = re.search(r' Parallel Run                    :', line)
        if match:

            if not runs or not runs[-1].slurm_id:
                runs.append(Run())
                run = runs[-1]

            t_recv = np.NAN
            t_waitR = np.NAN
            t_waitS = np.NAN
            t_pp = np.NAN

            continue

        if not runs:
            continue

        ##match = re.search(r'rank (\d+) is running on node (\S+)', line)
        ##if match:
            ##r = int(match.group(1))
            ##h = match.group(2)
            ##if not h in run.hosts:
                ##run.hosts[h] = []
            ##run.hosts[h].append(r)

            ##continue

        match = re.search(r'Transient run, dt    :  ([^\s]+)\s.*', line)
        if match:
            run.dt = float(match.group(1))

            continue

        match = re.search(r' Restart file time: ([^\s]+)', line)
        if match:
            time = float(match.group(1))
            run.t_start = time

            continue

        match = re.search(r'\s+step\s+iout\s+Time\s+CFL\s+:(.*)', line)
        if match:
            vars = match.group(1).split()
            var_order = {}
            for ii, v in enumerate(vars):
                var_order[v] = ii

            continue

        match = re.search(
            r'Receive total time:\s*(\S+), Receive waiting time:\s*(\S+)', line)
        if match:
            run.receiver = True
            t_recv = float(match.group(1))
            t_waitR = float(match.group(2))
            t_pp = t_recv - t_waitR

            continue

        match = re.search(
            r' Send waiting time:\s+(\S+).*', line)
        if match:
            run.sender = True
            t_waitS = float(match.group(1))

            continue

        match = re.search(r'(\d+)\s+(\S+)\s+(\S+)\s+(\S+)\s+:(.*)', line)
        if match:
            step = int(match.group(1))
            i_outer = int(match.group(2))
            time = float(match.group(3))
            # 4: CFL
            dyn = match.group(5).split()
            try:
                t_cpu = float(dyn[var_order['StepTime']])
            except:
                print(line)

            if not run.t_start:
                run.t_start = time

            run.stride = 1

            if run.t_start and run.dt:
                if len(run.step) > 0:
                    time = run.t_start + (step - run.step[0]) * run.dt
                else:
                    time = run.t_start

            if not np.isnan(t_recv):
                t_cpu = t_cpu - t_recv
            if not np.isnan(t_waitS):
                t_cpu = t_cpu - t_waitS

            if i_outer == 1:
                run.step.append(step)
                run.t.append(time)
                # exclude all steps which FOLLOW the exclude step and the first receive step
                if (args.exclude_every and (step % args.exclude_every == run.stride) or len(run.step) == 1) :
                    run.t_cpu.append(np.NAN)
                    run.t_waitR.append(np.NAN)
                    run.t_pp.append(np.NAN)
                    run.t_waitS.append(np.NAN)
                else:
                    run.t_cpu.append(t_cpu)
                    run.t_waitR.append(t_waitR)
                    run.t_pp.append(t_pp)
                    run.t_waitS.append(t_waitS)
            else:
                run.t_cpu[-1] = run.t_cpu[-1] + t_cpu

            t_recv = np.NAN
            t_waitR = np.NAN
            t_waitS = np.NAN
            t_pp = np.NAN

            continue

    runs = [x for x in runs if len(x.step) > 0]

    for run in runs:

        # TODO: int
        run.data = np.zeros(shape=(len(run.step),), dtype=[('step', 'int32'), ('global_step', 'int32'), (
            'time', 'float32'), ('cpu_time', 'float32'), ('waitR_time', 'float32'), ('waitS_time', 'float32'), ('pp_time', 'float32')])

        run.data['step'] = run.step
        run.data['global_step'] = run.step
        run.step = []
        run.data['time'] = run.t
        run.t = []
        run.data['cpu_time'] = run.t_cpu
        run.t_cpu = []
        run.data['waitR_time'] = run.t_waitR
        run.t_waitR = []
        run.data['waitS_time'] = run.t_waitS
        run.t_waitS = []
        run.data['pp_time'] = run.t_pp
        run.t_pp = []

    runs = drop_runs(runs)

    return runs


def plotCpuTimes(runs, args):

    plt.style.use(args.style)

    # if 'beamer' in args.style:
    # full beamer page
    fs = (0.25, 0.138)
    fs = (fs[0], fs[0] / 1.8209)
    fs = (fs[0] / 0.0254, fs[1] / 0.0254)
    # else:
    # tudreport with 1:2
    #fs = (0.17499796, 0.2464048)
    #fs = (fs[0], fs[0] * 0.5)
    #fs = (fs[0] / 0.0254, fs[1] / 0.0254)

    fig = plt.figure(figsize=(fs[0], fs[1]))
    #fig = plt.figure()

    ax = fig.add_subplot(111)

    if args.steps:
        x_var = 'global_step'
    else:
        x_var = 'time'

    plots = {}
    for ii, run in enumerate(runs):

        stride = run.stride
        data = run.data

        if args.total_t:
            total_t = np.nan_to_num(data['pp_time']) + np.nan_to_num(data['waitR_time']) + np.nan_to_num(data['waitS_time']) + data['cpu_time'] / (stride / args.per)
            inds = np.logical_not(np.isnan(total_t))
            plots['total_t'] = ax.scatter(data[x_var][inds],
                       total_t[inds],
                       color=list(mpl.rcParams['axes.prop_cycle'])[4]['color'],
                       marker='o',
                       linewidths=0,
                       label="_nolegend_" if 'total_t' in plots else "total time (per {} steps)".format(args.per))

        if args.wait_t:
            if run.receiver:
                inds = np.logical_not(np.isnan(data['waitR_time']))
                plots['waitR_time'] = ax.scatter(data[x_var][inds],
                           data['waitR_time'][inds],
                           color=list(mpl.rcParams['axes.prop_cycle'])[2]['color'],
                           marker='o',
                           linewidths=0,
                           label="_nolegend_" if 'waitR_time' in plots else "Receive wait time")
            if run.sender:
                inds = np.logical_not(np.isnan(data['waitS_time']))
                plots['waitS_time'] = ax.scatter(data[x_var][inds],
                           data['waitS_time'][inds],
                           color=list(mpl.rcParams['axes.prop_cycle'])[2]['color'],
                           marker='o',
                           linewidths=0,
                           label="_nolegend_" if 'waitS_time' in plots else "Send wait time")

        inds = np.logical_not(np.isnan(data['cpu_time']))
        plots['cpu_time'] = ax.scatter(data[x_var][inds],
                   data['cpu_time'][inds] / (stride / args.per),
                   color=list(mpl.rcParams['axes.prop_cycle'])[0]['color'],
                   marker='o',
                   linewidths=0,
                   label="_nolegend_" if 'cpu_time' in plots else "CPU time (per {} steps)".format(args.per))
        if args.pp_t:
            inds = np.logical_not(np.isnan(data['pp_time']))
            plots['pp_time'] = ax.scatter(data[x_var][inds],
                       data['pp_time'][inds],
                       color=list(mpl.rcParams['axes.prop_cycle'])[3]['color'],
                       marker='o',
                       linewidths=0,
                       label="_nolegend_" if 'pp_time' in plots else "PP time")

        N = int(args.avg / stride)
        if args.total_t:
            inds = np.logical_not(np.isnan(total_t))
            conv = np.convolve(total_t[inds], np.ones((N,)) / N, mode='valid')
            plots['total_t_conv'] = ax.step(data[x_var][inds][N - 1:],
                    conv,
                    color=list(mpl.rcParams['axes.prop_cycle'])[5]['color'],
                    label="_nolegend_" if 'total_t_conv' in plots else "total time (per {1} steps, {0:.0f} step average)".format(args.avg, args.per),
                    where="post")

        inds = np.logical_not(np.isnan(data['cpu_time']))
        conv = np.convolve(
            data['cpu_time'][inds] / (stride / args.per), np.ones((N,)) / N, mode='valid')
        plots['cpu_time_conv'] = ax.step(data[x_var][inds][N - 1:],
                conv,
                color=list(mpl.rcParams['axes.prop_cycle'])[1]['color'],
                label="_nolegend_" if 'cpu_time_conv' in plots else "CPU time (per {1} steps, {0:.0f} step average)".format(args.avg, args.per),
                where="post")

    (ymin, ymax) = ax.get_ylim()
    (xmin, xmax) = ax.get_xlim()
    xmin = max(xmin, 0)
    if args.xmin != None:
        xmin = args.xmin
    if args.xmax != None:
        xmax = args.xmax
    if args.ymin != None:
        ymin = args.ymin
    if args.ymax != None:
        ymax = args.ymax

    ax.set(
        xlabel=r'$t$ [s]',
        ylabel=r'CPU time [s]',
        ylim=(ymin, ymax),
        xlim=(xmin, xmax),
    )
    if args.steps:
        ax.set(xlabel=r'steps [-]',)

    if args.steps:
        for ii, run in enumerate(runs):

            i_start = run.data['global_step'][0]

            ax.axvline(i_start,
                color='grey',
                linewidth=1.0
                )

            if run.slurm_id:
                text = "run {} (SLURM-id {})".format(ii, run.slurm_id)
            else:
                text = "run {}".format(ii)

            ax.annotate(text,
                        xycoords='data',
                        xy=(i_start, 0.7 * (ymax - ymin)),
                        textcoords='offset points',
                        xytext=(2, 0),
                        horizontalalignment='left',
                        verticalalignment='top',
                        rotation=90,
                        color='grey')
    else:
        for ii, run in enumerate(runs):

            ax.axvline(run.t_start,
                color='grey',
                linewidth=1.0
                )

            if run.slurm_id:
                text = "run {} (SLURM-id {})".format(ii, run.slurm_id)
            else:
                text = "run {}".format(ii)

            ax.annotate(text,
                        xycoords='data',
                        xy=(run.t_start, 0.7 * (ymax - ymin)),
                        textcoords='offset points',
                        xytext=(2, 0),
                        horizontalalignment='left',
                        verticalalignment='top',
                        rotation=90,
                        color='grey')


    ax.grid(True)

    ax.legend(loc='best')

    plt.tight_layout()

    if args.out:
        print('writing {0}'.format(args.out))
        plt.savefig(args.out, dpi=300)

    if args.show:
        plt.show()


def plotWaitHis(data, args, title=None):

    data = data[np.logical_not(np.isnan(data))]

    nbins = int(round(args.xmax * 20))

    plt.style.use(args.style)

    fs = mpl.rcParams['figure.figsize']
    if "beamer" in args.style:
        fs[0] = fs[0] / 2
        fs[1] = fs[1] / 2
    else:
        fs[1] = fs[1] * 2 / 3

    fig = plt.figure(figsize=fs)
    #fig.suptitle(title)

    if args.splitax:
        gs = mpl.gridspec.GridSpec(4, 1)
        ax_b = plt.subplot(gs[1:, 0])
    else:
        ax_b = plt.subplot(111)

    weights = np.ones_like(data) / len(data)
    if args.precise:
        calc_name = "CFD"
    else:
        calc_name = "CAA"
    ax_b.hist(data, nbins, range=(0, args.xmax), weights=weights, label="{} Waiting Time".format(calc_name), color=list(mpl.rcParams['axes.prop_cycle'])[0]['color'])

    if args.splitax:
        ax_t = plt.subplot(gs[0, 0], sharex=ax_b)
        n, bins, patches = ax_t.hist(
            data, nbins,  range=(0, args.xmax), weights=weights, label="{} Waiting Time".format(calc_name), color=list(mpl.rcParams['axes.prop_cycle'])[0]['color'])

        ax_t.set_xlim(0, args.xmax)

        n_sorted = np.sort(n)
        # find the magnitude of n_max[-2]
        ndecimals = 0
        while True:
            if n_sorted[-2] / 10**(-1 * ndecimals) > 1:
                break
            ndecimals += 1

        # round up to the given number of decimals
        ymax_b = np.round(n_sorted[-2], decimals=ndecimals)
        if ymax_b < n_sorted[-2]:
            ymax_b = ymax_b + 10**(-1 * ndecimals)
        # and set the y limit
        ax_b.set_ylim(0, ymax_b)

        ymax_t = np.round(n_sorted[-1], decimals=ndecimals)
        if ymax_t < n_sorted[-1]:
            ymax_t = ymax_t + 10**(-1 * ndecimals)

        # set the ylimit so that the scaling is the same as the bottom axis
        # which is exactly 4 times longer
        ax_t.set_ylim(n_sorted[-1] - ax_b.get_ylim()[1] / 8, n_sorted[-1] + ax_b.get_ylim()[1] / 8)

        ticks = np.arange(0, 1 + ax_b.get_yticks()[1], ax_b.get_yticks()[1])
        ticks = ticks[ticks >= ax_t.get_ylim()[0]]
        ticks = ticks[ticks <= ax_t.get_ylim()[1]]
        ax_t.set_yticks(ticks)
        ax_t.get_yaxis().get_major_formatter().set_useOffset(False)

        ax_t.spines['bottom'].set_visible(False)
        ax_t.xaxis.tick_top()
        ax_t.tick_params(labeltop='off')
        ax_t.legend()

        ax_b.spines['top'].set_visible(False)
        ax_b.xaxis.tick_bottom()
    else:
        ax_b.legend()

    (xmin, xmax) = ax_b.get_xlim()
    ax_b.set(
        xlabel=r'Time [$\mathrm{s}$]',
        xlim=(0, xmax),
        #ylabel=r'Probability Density',
    )

    fig.text(0.03, 0.5, 'Probability Density Function', ha='center',
             va='center', rotation='vertical', fontsize=mpl.rcParams['ytick.labelsize'])


    if args.splitax:
        plt.subplots_adjust(
            left=0.16, right=0.975, bottom=0.225, top=0.95, hspace=0.5)


    return fig


def printSummaries(runs, args):

    for ii, run in enumerate(runs):

        print()
        print("-------------------------------")
        print("run {}:".format(ii))

        if run.slurm_id:
            print("SLURM-id: {}".format(run.slurm_id))

        print("Host distribution:")
        n_ranks = 0
        for h in sorted(run.hosts):
            n_ranks += len(run.hosts[h])
            print("\t{} ({} ranks) : {}".format(h, len(run.hosts[h]),run.hosts[h]))
        print("\t{} ranks in total".format(n_ranks))

        dta = run.data

        print("time statistics (mean, median, min, max, cumulated):")
        var_name = 'cpu_time'
        tmp = dta[var_name][np.logical_not(np.isnan(dta[var_name]))] / (run.stride / args.per)
        print("\t{} (per {} steps)  = {:10.2e}, {:10.2e}, {:10.2e}, {:10.2e}, {:10.2e}".format(var_name,
                args.per,
                np.mean(tmp),
                np.median(tmp),
                np.min(tmp),
                np.max(tmp),
                np.sum(tmp)
                ))
        if run.receiver:
            var_name = 'pp_time'
            tmp = dta[var_name][np.logical_not(np.isnan(dta[var_name]))]
            print("\t{} (per {} steps)  = {:10.2e}, {:10.2e}, {:10.2e}, {:10.2e}, {:10.2e}".format(var_name,
                args.per,
                np.mean(tmp),
                np.median(tmp),
                np.min(tmp),
                np.max(tmp),
                np.sum(tmp)
                ))
            var_name = 'waitR_time'
            tmp = dta[var_name][np.logical_not(np.isnan(dta[var_name]))]
            if tmp.shape[0] == 0:
                print("Warning: no waiting times found, try without '--exclude-every'")
            print("\t{} (per {} steps)  = {:10.2e}, {:10.2e}, {:10.2e}, {:10.2e}, {:10.2e}".format(var_name,
                args.per,
                np.mean(tmp),
                np.median(tmp),
                np.min(tmp),
                np.max(tmp),
                np.sum(tmp)
                ))
        if run.sender:
            var_name = 'waitS_time'
            tmp = dta[var_name][np.logical_not(np.isnan(dta[var_name]))]
            if tmp.shape[0] == 0:
                print("Warning: no waiting times found, try without '--exclude-every'")
            print("\t{} (per {} steps)  = {:10.2e}, {:10.2e}, {:10.2e}, {:10.2e}, {:10.2e}".format(var_name,
                args.per,
                np.mean(tmp),
                np.median(tmp),
                np.min(tmp),
                np.max(tmp),
                np.sum(tmp)
                ))

        print("steps: [{}, {}], di = {}".format(run.data['global_step'][0], run.data[
              'global_step'][-1], run.data['global_step'][-1] - run.data['global_step'][0]))

        print("time: [{}, {}] s, dt = {} s".format(
            run.data['time'][0],
            run.data['time'][-1],
            run.data['time'][-1] - run.data['time'][0],))

        print()


def main():
    parser = argparse.ArgumentParser(
        description='calculate and plot Mean CPU times of a simulation')
    parser.add_argument('--debug', action='store_true', dest='debug')
    parser.add_argument('--precise', action='store_true', help='parse PRECISE-UNS log files')

    subparsers = parser.add_subparsers(
        dest="subparser_name", title='subcommands', help='additional help')

    parser_stats = subparsers.add_parser('stats', help='stats mean')
    parser_stats.add_argument(
        'log_file', type=str, nargs='+', help='log files')
    parser_stats.add_argument('--exclude-every', type=int, help="exclude all steps which FOLLOW the nth step and the first receive step")
    parser_stats.add_argument('--per', type=int, default=1,
                              help="CPU time per N steps (default: %(default)s)")
    parser_stats.add_argument('--xmax', type=float, default=3)
    parser_stats.add_argument('--run', type=int, default=-1,
                              help="number of the considered run (default: %(default)s)")
    parser_stats.add_argument(
        '--style', default='default', help="plot style (default: %(default)s)")
    parser_stats.add_argument('--out', '-o', type=str)
    parser_stats.add_argument('--show', '-s', action='store_true')
    parser_stats.add_argument('--splitax', action='store_true')

    parser_plot = subparsers.add_parser('plot', help='plot CPU times')
    parser_plot.add_argument('log_file', type=str, nargs='+', help='log files')
    parser_plot.add_argument('--exclude-every', type=int, help="exclude all steps which FOLLOW the nth step and the first receive step")
    parser_plot.add_argument('--avg', type=int, default=100,
                             help="number of steps to average (default: %(default)s)")
    parser_plot.add_argument('--per', type=int, default=1,
                             help="CPU time per N steps (default: %(default)s)")
    parser_plot.add_argument('--steps', action='store_true')
    parser_plot.add_argument('--wait_t', action='store_true')
    parser_plot.add_argument('--pp_t', action='store_true')
    parser_plot.add_argument('--total_t', action='store_true')
    parser_plot.add_argument('--out', '-o', type=str)
    parser_plot.add_argument('--show', '-s', action='store_true')
    parser_plot.add_argument(
        '--style', default='default', help="plot style (default: %(default)s)")
    parser_plot.add_argument('--xmin', type=float)
    parser_plot.add_argument('--xmax', type=float)
    parser_plot.add_argument('--ymin', type=float)
    parser_plot.add_argument('--ymax', type=float)

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            level=logging.INFO,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )

    if not args.show and not args.out:
        args.show = True

    if args.precise:
        runs = parse_precise_log(args.log_file, args)
    else:
        runs = parse_nektar_log(args.log_file, args)

    printSummaries(runs, args)

    if args.subparser_name == "stats":
        figs = []
        if runs[args.run].receiver:
            f = plotWaitHis(runs[args.run].data['waitR_time'], args, "Receive Waiting Times")
            figs.append(("recv", f))
        if runs[args.run].sender:
            f = plotWaitHis(runs[args.run].data['waitS_time'], args, "Send Waiting Times")
            figs.append(("send", f))

        for tmp in figs:
            id, f = tmp
            if args.out:
                name, ext = os.path.splitext(args.out)
                fname = name + '_' +  id + ext
                print('writing {0}'.format(fname))
                f.savefig(fname, dpi=300)

        if args.show:
            plt.show()

    elif args.subparser_name == "plot":
        plotCpuTimes(runs, args)

    else:
        print('error')


if __name__ == "__main__":
    main()
