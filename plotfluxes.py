#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.




import re
import sys
import os
import argparse
import logging

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

logger = logging.getLogger('root')


def plot_fluxes(fluxes,
                var_name,
                style='default',
                xmin=None,
                xmax=None,
                ymin=None,
                ymax=None,
                out=None,
                show=True,
                title=None):

    plt.style.use(style)

    if 'beamer' in style:
        # full beamer page
        fs = (0.25, 0.138)
        fs = (fs[0], fs[0] / 1.8209)
        fs = (fs[0] / 0.0254, fs[1] / 0.0254)
    else:
        # tudreport with 1:2
        fs = (0.17499796, 0.2464048)
        fs = (fs[0], fs[0] * 0.5)
        fs = (fs[0] / 0.0254, fs[1] / 0.0254)

    fig = plt.figure(figsize=(fs[0], fs[1]))
    ax = fig.add_subplot(111)

    for flux in fluxes:
        ax.plot(
            flux['data']['Iterations'],
            flux['data'][var_name],
            label=flux['label'],
            color=list(mpl.rcParams['axes.prop_cycle'])[flux['color']]['color']
        )

    (ymin_new, ymax_new) = ax.get_ylim()
    (xmin_new, xmax_new) = ax.get_xlim()
    xmin_new = max(xmin_new, 0)
    if xmin:
        xmin_new = xmin
    if xmax:
        xmax_new = xmax
    if ymin:
        ymin_new = ymin
    if ymax:
        ymax_new = ymax

    ax.set(
        xlabel=r'$i$ [-]',
        ylim=(ymin_new, ymax_new),
        xlim=(xmin_new, xmax_new),
    )
    ax.set(ylabel='{}'.format(var_name))
    ax.grid(True)
    ax.legend(loc='upper center', ncol=8, mode='expand')

    if title:
        ax.set(title=title)

    plt.tight_layout()

    if out:
        for o in out:
            print('writing {0}'.format(o))
            plt.savefig(o, bbox_inches='tight')

    if show:
        plt.show()


def main():
    parser = argparse.ArgumentParser(
        description='plots the fluxes from a PRECISE-UNS flx file over time'
    )
    parser.add_argument('-g', '--group', type=str, nargs='+', action='append',
                        required=True, help='NAME COLOR_NUM FILE')
    parser.add_argument('--out', '-o', type=str, action='append')
    parser.add_argument('--show', '-s', action='store_true', dest='show')
    parser.add_argument('--style', action='store', dest='style',
                        default='default', help="plot style (default: %(default)s)")
    parser.add_argument('--debug', action='store_true', dest='debug')
    parser.add_argument('--title', type=str)
    parser.add_argument('--xmin', dest='xmin', type=float)
    parser.add_argument('--xmax', dest='xmax', type=float)
    parser.add_argument('--ymin', dest='ymin', type=float)
    parser.add_argument('--ymax', dest='ymax', type=float)
    parser.add_argument('--var', default='Outflow', type=str, help="variable to plot (default: %(default)s)")

    args = parser.parse_args()

    if not args.show and not args.out:
        args.show = True

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            level=logging.INFO,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )

    f_groups = []

    for  group in args.group:

        if len(group) < 3:
            print("group too short")
            sys.exit(-1)

        label = group[0]
        color = int(group[1])
        fn = group[2]

        fluxes = np.genfromtxt(
            fn,
            names=True,
            #skip_header=1 + skip_rows,
            autostrip=True
        )

        # conver dtypeof Iterations
        new_types = fluxes.dtype.descr
        new_types[0] = (new_types[0][0], '<i8')
        new_types = np.dtype(new_types)
        fluxes = fluxes.astype(new_types)

        # drop duplicate Iterations
        fluxes = np.unique(fluxes[-1:0:-1])[-1:0:-1]

        f_groups.append(
            {'label': label,
             'data': fluxes,
             'color': color}
            )

        vars = fluxes.dtype.names
        print("available vars: {} in {}".format(', '.join(vars), fn))

        if args.var not in vars:
            print("\"{}\" not in vars".format(args.var))
            sys.exit(-1)

    plot_fluxes(f_groups,
                var_name=args.var,
                style=args.style,
                xmin=args.xmin,
                xmax=args.xmax,
                ymin=args.ymin,
                ymax=args.ymax,
                out=args.out,
                show=args.show,
                title=args.title)


if __name__ == "__main__":
    main()
