#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.



import sys
import os
import argparse
import logging
import natsort

import numpy as np
import h5py

logger = logging.getLogger('root')
args = None

def join_sections(sections, n_mic, overlap = False):

    n_mic = '{}'.format(n_mic)

    if not overlap:
        # drop sections which are subseeded by sections starting earlier than themselves
        while True:
            droplist = []
            for ii in range(len(sections) - 1):
                if sections[ii][n_mic]['time'][0] >= sections[ii + 1][n_mic]['time'][0]:
                    droplist.append(ii)

            if not droplist:
                break

            for ii in reversed(droplist):
                sections.pop(ii)

    starts = []
    for section in sections:
        starts.append(section[n_mic]['time'][0])

    stop_ids = []

    for ii in range(len(sections) - 1):
        # find the index where the next section starts
        tmp = np.abs(sections[ii][n_mic]['time'] - starts[ii + 1])
        idx = np.argmin(tmp)
        stop_ids.append(idx)

    stop_ids.append(-1)

    sections_sliced = []
    for ii in range(len(sections)):
        if not overlap:
            sections_sliced.append(sections[ii][n_mic][0:stop_ids[ii]])
        else:
            sections_sliced.append(sections[ii][n_mic][0:-1])

    # reduce fields to common entries
    names = set(sections_sliced[0].dtype.names)
    for sec in sections_sliced:
        names = set(sec.dtype.names).intersection(names)
    names = list(names)
    for ii, sec in enumerate(sections_sliced):
        sections_sliced[ii] = sections_sliced[ii][names]

    joint_mic = np.concatenate(sections_sliced, axis=0)

    return joint_mic


def open_mic_files(mic_files):

    file_handles = []
    mic_groups = []

    for fn in mic_files:
        f = h5py.File(fn, "r")
        file_handles.append(f)
        mic_groups.append(f['mics'])

    return mic_groups, file_handles


def main():
    parser = argparse.ArgumentParser(
        description='Joins one or more HDF5 history files')
    parser.add_argument('in_files', nargs='+', type=str, help='in file (hdf5)')
    parser.add_argument('out_file', type=str, help='out file (hdf5)')
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--downsample', type=int, default=1, help="use only every Nth sample")


    global args
    args = parser.parse_args()


    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            level=logging.INFO,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )

    out_file = h5py.File(args.out_file, "w")
    out_group = out_file.create_group("mics")

    args.in_files = natsort.natsorted(args.in_files)
    mic_groups, mic_handles = open_mic_files(args.in_files)

    for n_mic in mic_groups[0]:
        joint_mic = join_sections(mic_groups, int(n_mic))
        joint_mic = joint_mic[::args.downsample]
        mic_dset = out_group.create_dataset('{}'.format(n_mic), data=joint_mic)
        if 'location' in mic_groups[0][n_mic].attrs:
            mic_dset.attrs['location'] = mic_groups[0][n_mic].attrs['location']

    out_file.close()
    for fh in mic_handles:
        fh.close()

if __name__ == "__main__":
    main()
