#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  This file is part of nektar-tools.
#
#  Copyright 2018 Kilian Lackhove
#
#  nektar-tools is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  nektar-tools is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with nektar-tools.  If not, see <http://www.gnu.org/licenses/>.


import sys
import os
import argparse
import logging

import numpy as np

from scipy.spatial import ConvexHull

logger = logging.getLogger('root')
np.set_printoptions(threshold=np.nan)


class Mesh:

    def __init__(self):

        self.types = {'line': 1, 'tri': 2, 'quad': 3,
                      'tet': 4, 'hex': 5, 'prism': 6, 'pyra': 7}
        self.types_r = {1: 'line', 2: 'tri', 3: 'quad',
                        4: 'tet', 5: 'hex', 6: 'prism', 7: 'pyra'}
        self.n_nodes = {'line': 2, 'tri': 3, 'quad': 4,
                        'tet': 4, 'hex': 8, 'prism': 6, 'pyra': 5}

        # format: [(x, y, z),  (x, y, z),  ...]
        self.vertices = np.zeros(0, dtype=np.double)
        # format: [(vid0, vid1, vid2, ...), (vid0, vid1, vid2, ...), ...]
        self.elm_verts = np.zeros(0, dtype=np.int32)
        # format: [physid, physid, ...]
        self.elm_physid = np.zeros(0, dtype=np.int16)
        # format: [type, type, ...]
        self.elm_type = np.zeros(0, dtype=np.int16)
        # format: [(x, y, z),  (x, y, z),  ...]
        self.elm_centers = np.zeros(0, dtype=np.double)
        # format: [vol, vol, ...]
        self.elm_vols = np.zeros(0, dtype=np.double)

    def scale(self, fact):
        self.vertices = fact * self.vertices


class MshMeshReader:

    """
    Reads a Gmsh msh file (version 2)
    """

    def Read(self, filename):

        mesh = Mesh()

        with open(filename, 'r') as fn:

            lines_nodes = False
            line_elements = False
            intro_line = False

            elements = None

            while True:
                line = fn.readline()
                if line == '':
                    break

                if line.startswith('$Nodes'):
                    lines_nodes = True
                    intro_line = True
                    continue
                if line.startswith('$EndNodes'):
                    lines_nodes = False
                    continue

                if line.startswith('$Elements'):
                    line_elements = True
                    intro_line = True
                    continue
                if line.startswith('$EndElements'):
                    line_elements = False
                    continue

                if lines_nodes:
                    if intro_line:
                        mesh.vertices = np.zeros(
                            shape=(int(line), 3), dtype=np.double)
                        intro_line = False
                        continue

                    e = line.split(" ")
                    num = int(e[0]) - 1
                    mesh.vertices[num, 0] = e[1]
                    mesh.vertices[num, 1] = e[2]
                    mesh.vertices[num, 2] = e[3]

                    continue

                if line_elements:
                    if intro_line:
                        mesh.elm_type = np.zeros(int(line), dtype=np.int32)
                        mesh.elm_physid = np.zeros(int(line), dtype=np.int16)
                        mesh.elm_verts = np.zeros(
                            shape=(int(line), 8), dtype=np.int32)
                        intro_line = False
                        continue

                    e = line.split(" ")
                    e = [int(k) for k in e]
                    num = e[0] - 1
                    type_id = e[1]
                    num_tags = e[2]

                    n_nodes = mesh.n_nodes[mesh.types_r[type_id]]

                    mesh.elm_type[num] = type_id
                    mesh.elm_physid[num] = e[3]  # hardcoded to one tag

                    for i in range(0, n_nodes):
                        mesh.elm_verts[num, i] = e[3 + num_tags + i] - 1

                    continue

        return mesh


class CgnsMeshReader:

    """
    Reads a CGNS mesh (Version 3.1 HDF) as written by ANSYS icemcfd with the following options:
    - Input Grid Type: Unstructured
    - Default BC patches: No
    - Entities to use for BC patch: Face Elements
    - Bar and Node elements: No
    - Version: 3.1 HDF
    """

    def __init__(self):

        self.cgns_types_r = {
            1: 'Null',
            2: 'NODE',
            3: 'BAR_2',
            4: 'BAR_3',
            5: 'TRI_3',
            6: 'TRI_6',
            7: 'QUAD_4',
            8: 'QUAD_8',
            9: 'QUAD_9',
            10: 'TETRA_4',
            11: 'TETRA_10',
            12: 'PYRA_5',
            13: 'PYRA_14',
            14: 'PENTA_6',
            15: 'PENTA_15',
            16: 'PENTA_18',
            17: 'HEXA_8',
            18: 'HEXA_20',
            19: 'HEXA_27',
            20: 'MIXED',
            21: 'NGON_n',
            22: 'UserDefined'}

        self.cgns_types = {v: k for k, v in self.cgns_types_r.items()}

        self.cgns_n_nodes = {
            'Null': 1,
            'NODE': 1,
            'BAR_2': 2,
            'BAR_3': 3,
            'TRI_3': 3,
            'TRI_6': 6,
            'QUAD_4': 4,
            'QUAD_8': 8,
            'QUAD_9': 9,
            'TETRA_4': 4,
            'TETRA_10': 10,
            'PYRA_5': 5,
            'PYRA_14': 14,
            'PENTA_6': 6,
            'PENTA_15': 15,
            'PENTA_18': 18,
            'HEXA_8': 8,
            'HEXA_20': 20,
            'HEXA_27': 27,
        }

        self.cgns_to_gmsh = {
            1: 0,
            2: 0,
            3: 1,
            4: 0,
            5: 2,
            6: 0,
            7: 3,
            8: 0,
            9: 0,
            10: 4,
            11: 0,
            12: 7,
            13: 0,
            14: 6,
            15: 0,
            16: 0,
            17: 5,
            18: 0,
            19: 0,
        }

    def Read(self, filename):

        import h5py

        mesh = Mesh()
        #mesh.types = {'line': 1, 'tri': 2, 'quad': 3,}
        print('Reading CGNS mesh')

        mesh_file = h5py.File(filename, "r", driver='core')

        for z in mesh_file["BASE#1"]:
            # we support only one zone
            zone = mesh_file["BASE#1" + "/" + z]

        groups = []
        for z in zone:

            if z == " data":
                continue

            label = zone[z].attrs['label'].decode('UTF-8')
            if label == "GridCoordinates_t":
                grid_coordinates = zone[z]
            elif label == "Elements_t":
                if zone[z][' data'][0] == self.cgns_types['Null']:
                    pass
                elif zone[z][' data'][0] == self.cgns_types['NODE']:
                    pass
                else:
                    groups.append(zone[z])

        print("Found {} groups".format(len(groups)))

        print("Reading vertices")
        # workaround to force h5py to read the entire array into memory
        # otherwise, the transpose operation would take forever
        verts_x = np.array(grid_coordinates['CoordinateX/ data']) * 1
        verts_y = np.array(grid_coordinates['CoordinateY/ data']) * 1
        verts_z = np.array(grid_coordinates['CoordinateZ/ data']) * 1

        mesh.vertices = np.transpose(np.array([verts_x, verts_y, verts_z]))

        # find number of elements
        tot_elms = 0
        for g in groups:
            tot_elms += g["ElementRange/ data"][1] - \
                g["ElementRange/ data"][0] + 1
        print("Found {} elements".format(tot_elms))

        mesh.elm_verts = np.ones(shape=(tot_elms, 3), dtype=np.int32) * -100
        mesh.elm_physid = np.ones(shape=(tot_elms, ), dtype=np.int16) * -100
        mesh.elm_type = np.ones(shape=(tot_elms, ), dtype=np.int16) * -100

        print("mappings:")
        elm_pos = 0
        phys_id = 1
        for g in groups:
            group_type = self.cgns_types_r[g[" data"][0]]

            if group_type == 'MIXED':
                elm_verts, types = self.ReadConnecMixed(g)
            else:
                elm_verts, types = self.ReadConnec(g)

            nverts = elm_verts.shape[1]
            nelms = elm_verts.shape[0]

            if nverts > mesh.elm_verts.shape[1]:
                # grow the array to hold all the vertices. this trades speed
                # for memory
                newshape = (
                    mesh.elm_verts.shape[0], nverts - mesh.elm_verts.shape[1])
                mesh.elm_verts = np.c_[mesh.elm_verts,
                                       np.ones(shape=newshape, dtype=np.int32)]

            mesh.elm_verts[elm_pos:elm_pos+nelms, 0:nverts] = elm_verts
            mesh.elm_physid[elm_pos:elm_pos+nelms] = phys_id
            mesh.elm_type[elm_pos:elm_pos+nelms] = types

            print("\t{} -> {}".format(phys_id,
                                      g.attrs['name'].decode('UTF-8')))

            elm_pos += nelms
            phys_id += 1

        print('finished mesh setup')

        mesh_file.close()

        return mesh

    def ReadConnec(self, g):
        cgns_type = self.cgns_types_r[g[" data"][0]]
        type_id = self.cgns_types[cgns_type]
        connec = g["ElementConnectivity/ data"]

        nodes_per_el = self.cgns_n_nodes[cgns_type]
        n_elms = int(connec.shape[0]/nodes_per_el)

        types = np.ones(shape=(n_elms,), dtype=np.int16) * \
            self.cgns_to_gmsh[type_id]

        return connec[()].reshape((n_elms, nodes_per_el)), types

    def ReadConnecMixed(self, g):

        n_elms = g["ElementRange/ data"][1] - g["ElementRange/ data"][0] + 1

        data = g["ElementConnectivity/ data"]
        connec = np.ones(shape=(n_elms, 8), dtype=data.dtype) * -100
        types = np.ones(shape=(n_elms, ), dtype=np.int16) * -100
        con_pos = 0
        dat_pos = 0

        max_n_verts = 0

        while con_pos < n_elms:
            cgns_type = self.cgns_types_r[data[dat_pos]]
            type_id = self.cgns_types[cgns_type]
            n_verts = self.cgns_n_nodes[cgns_type]

            max_n_verts = max(max_n_verts, n_verts)

            connec[con_pos][0:n_verts] = data[dat_pos+1:dat_pos+1+n_verts]
            types[con_pos] = self.cgns_to_gmsh[type_id]
            dat_pos = dat_pos + 1 + n_verts
            con_pos += 1

        return np.delete(connec, np.s_[max_n_verts:], 1), types


class GmshMeshWriter:

    def Write(self, mesh, filename):

        self.mesh = mesh

        with open(filename, 'w') as fn:

            print("$MeshFormat", file=fn)
            print("2.2 0 8", file=fn)
            print("$EndMeshFormat", file=fn)

            print("$Nodes", file=fn)
            print(mesh.vertices.shape[0], file=fn)

            print("Writing Nodes")
            for ii, vert in enumerate(mesh.vertices):
                print("{} {} {} {}".format(
                    ii + 1, vert[0], vert[1], vert[2]), file=fn)
            print("$EndNodes", file=fn)

            print("Writing Elements")
            print("$Elements", file=fn)
            print(mesh.elm_verts.shape[0], file=fn)
            ii = 0
            for el_id in range(0, mesh.elm_verts.shape[0]):

                type_id = mesh.elm_type[el_id]

                tags = "{}".format(mesh.elm_physid[el_id])
                num_tags = 1

                el_type = mesh.types_r[type_id]
                verts = mesh.elm_verts[el_id][0:mesh.n_nodes[el_type]]
                nodes = " ".join("{}".format(e) for e in verts)

                print("{} {} {} {} {}".format(
                    el_id + 1, type_id, num_tags, tags, nodes), file=fn)

                ii += 1

            print("$EndElements", file=fn)


class StarMeshWriter:

    """
    Writes star-cd ascii files that can be read by the PRECISE-UNS preprocessor.
    This writer expects the fluid to be assigned the physical id 0.
    """

    def Write(self, mesh, filename):

        with open(filename + ".vrt", 'w') as fn:

            print("Writing {} Nodes".format(mesh.vertices.shape[0]))
            for ii, vert in enumerate(mesh.vertices):
                print("{: >9d}      {: >16.9e}{: >16.9e}{: >16.9e}".format(
                    ii + 1, vert[0], vert[1], vert[2]), file=fn)

        with open(filename + ".cel", 'w') as fn:

            print("Writing Cells")

            if not 0 in mesh.elm_physid:
                print("No cells found. Make sure the fluid is assigned physical ID 0")

            ii = 0
            for el_id in range(0, mesh.elm_verts.shape[0]):

                phys_id = mesh.elm_physid[el_id]

                type_id = mesh.elm_type[el_id]
                el_type = mesh.types_r[type_id]
                verts = np.zeros(shape=(8,), dtype=np.int32)
                verts[0:mesh.n_nodes[el_type]
                      ] = mesh.elm_verts[el_id][0:mesh.n_nodes[el_type]] + 1
                nodes = "".join("{: >9d}".format(e) for e in verts)

                if phys_id == 0:
                    ctid = 6
                else:
                    ctid = 7

                print("{: >9d}      {}{: >9d}{: >5d}{: >5d}".format(
                    el_id + 1, nodes, ctid, -1, -1), file=fn)

                ii += 1

        with open(filename + ".bnd", 'w') as fn:

            print("Writing Boundary Faces")
            ii = 0
            for el_id in range(0, mesh.elm_verts.shape[0]):

                phys_id = mesh.elm_physid[el_id]

                # we expect the volume in phys id 0!
                if phys_id == 0:
                    continue

                type_id = mesh.elm_type[el_id]
                el_type = mesh.types_r[type_id]

                verts = np.zeros(shape=(4,), dtype=np.int32)
                verts[0:mesh.n_nodes[el_type]
                      ] = mesh.elm_verts[el_id][0:mesh.n_nodes[el_type]] + 1
                nodes = "".join("{: >9d}".format(e) for e in verts)

                print("{: >8d}      {}{: >7d}{: >7d}{: >11s}".format(
                    el_id + 1, nodes, phys_id, 0, "phys{}".format(phys_id)), file=fn)

                ii += 1


def main():
    parser = argparse.ArgumentParser(
        description='converts a CGNS mesh (3.1 HDF) to gmsh format')
    parser.add_argument('input', type=str, help="input file")
    parser.add_argument('output', type=str, help="input file")
    parser.add_argument('--input-type', type=str,
                        help="input file type (default: %(default)s)", default="cgns")
    parser.add_argument('--output-type', type=str,
                        help="input file type (default: %(default)s)", default="gmsh")
    parser.add_argument('--scale', type=float, default=1.0,
                        help="scale mesh (default: %(default)s)")
    parser.add_argument('--debug', action='store_true', dest='debug')

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )
    else:
        logging.basicConfig(
            format='%(asctime)s %(levelname)-8s %(module)-12s %(funcName)-12s %(lineno)-5d %(message)s'
        )

    if args.input_type.lower() == "cgns":
        reader = CgnsMeshReader()
    elif args.input_type.lower() == "gmsh":
        reader = MshMeshReader()
    else:
        print("unsupported input format")
        sys.exit(-1)
    mesh = reader.Read(args.input)

    mesh.scale(args.scale)

    if args.output_type.lower() == "gmsh":
        writer = GmshMeshWriter()
    elif args.output_type.lower() == "star":
        writer = StarMeshWriter()
    else:
        print("unsupported output format")
        sys.exit(-1)
    writer.Write(mesh, args.output)


if __name__ == "__main__":
    main()
